<?php

function getArticles() {
    return [
        '1' => [
            'id' => 1,
            'title' => "First Post",
            'content' => "dolor ipsum etc"
        ],
        '20' => [
            'id' => 2,
            'title' => "Second Post",
            'content' => "dolor2 ipsum2 etc2"
        ],
        '7' => [
            'id' => 3,
            'title' => "Third Post",
            'content' => "dolor3 ipsum3 etc3"
        ]
    ];
}

function GetArticleById($id) {
    $articles = getArticles();

    return $articles[$id];
}
?>