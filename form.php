<?php

$isSent = false;
$err = '';
$name = '';
$phone = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    $name = trim($_POST['name']);
    $phone = $_POST['phone'];

    $isSent = true;

    if ($name == '' || $phone == ''){
        $err = 'Fill all fields';
        $isSent = false;
    }
}
?>

<div class="form">
    <? if($isSent): ?>
        <p>Form sent by POST</p>
        <p><?=$name ?></p>
        <p><?=$phone ?></p>
    <? else: ?>
    <? if($err != ''): ?>
        <p><?=$err?></p>
    <? endif ?>
        <form action='form.php' method="post">
            Name <input type="text" name="name" value='<?=$name?>'></br>
            Phone <input type="text" name="phone" value='<?=$phone?>'>
            <button>Send</button>
        </form>
    <? endif ?>
</div>