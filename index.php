<?php 

include_once ("functions.php");

$articles = getArticles();

?>

<div class="articles">
    <? foreach ($articles as $id => $article): ?>
        <div class="article">
            <h2><?=$article['title'] ?></h2>
            <div class="post">
                <a href="articles.php?id=<?=$id?>">Read more</a>
            </div>
        </div>
    <? endforeach ?>
</div>